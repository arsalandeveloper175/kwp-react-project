
//   Home Page JS

$(function () {

if($(window).width() <= 768){
    if(('.coomon-responsove-slider').length != 0){
        $('.coomon-responsove-slider').addClass('owl-carousel owl-theme');
        $('.coomon-responsove-slider').owlCarousel({
            loop:false,
            margin:10,
            dots: true,
            autoplay:false,
            autoplayTimeout:2000,
            autoplayHoverPause:false,
            mouseDrag: true,
            stagePadding:5,
            responsive:{
                0:{

                    items:1
                }, 
                480:{

                    items:1
                },
                577:{

                    items:1
                },
                767:{
                    items:2
                }
            }
        });
    }
}

if($(window).width() <= 768){
    if(('.coomonone-responsove-slider').length != 0){
        $('.coomonone-responsove-slider').addClass('owl-carousel owl-theme');
        $('.coomonone-responsove-slider').owlCarousel({
            loop:false,
            margin:10,
            dots: true,
            autoplay:false,
            autoplayTimeout:2000,
            autoplayHoverPause:false,
            mouseDrag: true,
            stagePadding:5,
            responsive:{
                0:{

                    items:1
                }, 
                480:{

                    items:1
                },
                577:{

                    items:1
                },
                767:{
                    items:2
                }
            }
        });
    }
}



if($(window).width() <= 993){
    if(('.coomonthre-responsove-slider').length != 0){
        $('.coomonthre-responsove-slider').addClass('owl-carousel owl-theme');
        $('.coomonthre-responsove-slider').owlCarousel({
            loop:false,
            margin:10,
            dots: true,
            autoplay:false,
            autoplayTimeout:2000,
            autoplayHoverPause:false,
            mouseDrag: true,
            stagePadding:5,
            responsive:{
                0:{

                    items:1
                }, 
                480:{

                    items:1
                },
                577:{

                    items:1
                },
                766:{
                    items:1
                },
                992:{
                    items:2
                }
            }
        });
    }
}



$(window).scroll(function(){
    var scrollheader  = $(window).scrollTop() > 50;
    
    if(scrollheader){
        $(".main-header").addClass("header-sticky");
    }
    else{
        $(".main-header").removeClass("header-sticky");
    }
   
});

$(document).ready(function(){
	$('#nav-icon3').click(function(){
		$(this).toggleClass('open');
	});
});

$('.nav-center .nav-item .nav-link').click(function(){
    $('.nav-center .nav-item .nav-link').removeClass("active");
    $(this).addClass("active");
});




AOS.init({
    disable: function() {
        return window.innerWidth < 800;
    }
});



});
