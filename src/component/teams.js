import React from "react";
//Owl Carousel Libraries and Module
import OwlCarousel from 'react-owl-carousel';  
import 'owl.carousel/dist/assets/owl.carousel.css';  
import 'owl.carousel/dist/assets/owl.theme.default.css';  

const Teams = () =>{
    const teamslider = {
        loop:false,
        margin:10,
        dots: false,
        autoplay:false,
        autoplayTimeout:2000,
        autoplayHoverPause:false,
        mouseDrag: false,
        responsive:{
            0:{
                items:1,
                dots:true,
            }, 
            480:{
                items:1,
                dots:true,
            },
            577:{
                items:1,
                dots:true,
            },
            767:{
                items:1,
                dots:true,
            },
            992:{
                items:2,
            },
            1199:{
                items:3,
            }
        }
    }
    return(
        <>
            
        <section className="banner-header">
            <div className="container text-center">
                <h1 className="sec-headings">The Team</h1>
            </div>
        </section>

        <section className="team-grids">
            <div className="container">
                <OwlCarousel  className="row coomonthre-responsove-slider owl-carousel" {...teamslider}> 
                    <div className="project-slide-item">
                        <div className="team-box">
                            <div className="img-tem-box">
                                <img src="img/team/1.jpg" alt=""/>
                                <div className="tes-cont">
                                    <h6>Role:</h6>
                                    <p>Project Leader | Research Interests: Water Systems Analysis, Water Pricing</p>
                                    <h6> Contact:</h6>
                                    <p>hassaan.khan@sse.habib.edu.pk</p>
                                    <div className="flip-icon">
                                        <img src="img/hvr-flip.svg" alt="" className="hvr-flip"/>
                                    </div>
                                </div>
                            </div>
                            <p>
                                <span>Dr. Hassaan Furqan Khan</span>
                                Principal Investigator
                            </p>
                        </div>
                    </div>
                    <div className="project-slide-item">
                        <div className="team-box">
                            <div className="img-tem-box">
                                <img src="img/team/2.jpg" alt=""/>
                                <div className="tes-cont">
                                    <h6>Role:</h6>
                                    <p> Advisor, Socioeconomic Studies | Research Interests: City and Regional Planning, Climate Change, Infrastructures, Inequality</p>
                                    <h6> Contact:</h6>
                                    <p>nhanwar@iba.edu.pk</p>
                                    <div className="flip-icon">
                                        <img src="img/hvr-flip.svg" alt="" className="hvr-flip"/>
                                    </div>
                                </div>
                            </div>
                            <p>
                                <span>Dr. Nausheen H. Anwar</span>
                                Co-Principal Investigator
                            </p>
                        </div>
                    </div>
                    <div className="project-slide-item">
                        <div className="team-box">
                            <div className="img-tem-box">
                                <img src="img/team/3.jpg" alt=""/>
                                <div className="tes-cont">
                                    <h6>Role:</h6>
                                    <p>Technical Lead (Smart Flowmeter Development) | Research Interests: Systems Design, Fluid flow Measurement, Wireless Sensor Networks and Internet of Things </p>
                                    <h6> Contact:</h6>
                                    <p> junaid.memon@sse.habib.edu.pk</p>
                                    <div className="flip-icon">
                                        <img src="img/hvr-flip.svg" alt="" className="hvr-flip"/>
                                    </div>
                                </div>
                            </div>
                            <p>
                                <span>Junaid Ahmed Memon</span>
                                Co-Principal Investigator
                            </p>
                        </div>
                    </div>
                </OwlCarousel>
                <OwlCarousel  className="row coomonthre-responsove-slider owl-carousel" {...teamslider}> 
                    <div className="project-slide-item">
                        <div className="team-box">
                            <div className="img-tem-box">
                                <img src="img/team/4.jpg" alt=""/>
                                <div className="tes-cont">
                                    <h6>Role:</h6>
                                    <p>Lead, Water Pricing | Research Interests: Labor Economics, Applied Econometrics, Development Economics</p>
                                    <h6> Contact:</h6>
                                    <p>sana.khalil@ahss.habib.edu.pk</p>
                                    <div className="flip-icon">
                                        <img src="img/hvr-flip.svg" alt="" className="hvr-flip"/>
                                    </div>
                                </div>
                            </div>
                            <p>
                                <span>Sana Khalil</span>
                                Faculty Researcher
                            </p>
                        </div>
                    </div>
                    <div className="project-slide-item">
                        <div className="team-box">
                            <div className="img-tem-box">
                                <img src="img/team/5.jpg" alt=""/>
                                <div className="tes-cont">
                                    <h6>Role:</h6>
                                    <p>Advisor, IoT | Research Interests: Cellular Networks, Internet of Things, Computer Networking, and Wireless Communications.</p>
                                    <h6> Contact:</h6>
                                    <p>moiz.anis@sse.habib.edu.pk</p>
                                    <div className="flip-icon">
                                        <img src="img/hvr-flip.svg" alt="" className="hvr-flip"/>
                                    </div>
                                </div>
                            </div>
                            <p>
                                <span>Dr. Moiz Anis</span>
                                Faculty Researcher
                            </p>
                        </div>
                    </div>
                    <div className="project-slide-item">
                        <div className="team-box">
                            <div className="img-tem-box">
                                <img src="img/team/6.jpg" alt=""/>
                                <div className="tes-cont">
                                    <h6>Role:</h6>
                                    <p>Flowmeter Hardware Development | Research Interests: Hydroinformatics, Embedded Systems, Water Metering</p>
                                    <h6> Contact:</h6>
                                    <p>abdul.soomro@sse.habib.edu.pk</p>
                                    <div className="flip-icon">
                                        <img src="img/hvr-flip.svg" alt="" className="hvr-flip"/>
                                    </div>
                                </div>
                            </div>
                            <p>
                                <span>Abdul Rehman Soomro</span>
                                Research Assistant
                            </p>
                        </div>
                    </div>
                </OwlCarousel>
                <OwlCarousel  className="row coomonthre-responsove-slider owl-carousel" {...teamslider}> 
                    <div className="project-slide-item">
                        <div className="team-box">
                            <div className="img-tem-box">
                                <img src="img/team/7.jpg" alt=""/>
                                <div className="tes-cont">
                                    <h6>Role:</h6>
                                    <p>Lyari Study | Research Interests: Urban Planning, Climate Change Mitigation, Ecological Justice</p>
                                    <h6> Contact:</h6>
                                    <p>ali.arshad@sse.habib.edu.pk</p>
                                    <div className="flip-icon">
                                        <img src="img/hvr-flip.svg" alt="" className="hvr-flip"/>
                                    </div>
                                </div>
                            </div>
                            <p>
                                <span>Syed Ali Arshad</span>
                                Research Assistant
                            </p>
                        </div>
                    </div>
                    <div className="project-slide-item">
                        <div className="team-box">
                            <div className="img-tem-box">
                                <img src="img/team/8.jpg" alt=""/>
                                <div className="tes-cont">
                                    <h6>Role:</h6>
                                    <p>Full Stack Developer | Research Interests: Computer Vision, Machine Learning Modelling</p>
                                    <h6> Contact:</h6>
                                    <p>hiba.jamal@habib.edu.pk</p>
                                    <div className="flip-icon">
                                        <img src="img/hvr-flip.svg" alt="" className="hvr-flip"/>
                                    </div>
                                </div>
                            </div>
                            <p>
                                <span>Hiba Jamal</span>
                                Research Assistant
                            </p>
                        </div>
                    </div>
                </OwlCarousel>
            </div>
        </section>


        <section className="research-area">
            <div className="container">
                <h3>Undergraduate Researchers</h3>
                <div className="research-table">
                    <div className="tab-row">
                        <h6>Name</h6>
                        <h6>Major</h6>
                        <h6>Graduating Year</h6>
                    </div>
                    <div className="tab-row">
                        <p>Sarwan Shah</p>
                        <p>Electrical Engineering</p>
                        <p>2021</p>
                    </div>
                    <div className="tab-row">
                        <p>Neha Khatri	</p>
                        <p>Social Development and Policy</p>
                        <p>2021</p>
                    </div>
                    <div className="tab-row">
                        <p>Aoun Hussain	</p>
                        <p>Electrical Engineering</p>
                        <p>2021</p>
                    </div>
                    <div className="tab-row">
                        <p>Ahsan Ali</p>
                        <p>Electrical Engineering</p>
                        <p>2022</p>
                    </div>
                    <div className="tab-row">
                        <p>Daniyal Saeed</p>
                        <p>Electrical Engineering</p>
                        <p>2022</p>
                    </div>
                    <div className="tab-row">
                        <p>Hunaina Khan	</p>
                        <p>Social Development and Policy</p>
                        <p>2022</p>
                    </div>
                    <div className="tab-row">
                        <p>Markhan Mushtaque</p>
                        <p>Social Development and Policy</p>
                        <p>2022</p>
                    </div>
                    <div className="tab-row">
                        <p>Muhammad Hamza Raza</p>
                        <p>Social Development and Policy</p>
                        <p>2022</p>
                    </div>
                    <div className="tab-row">
                        <p>Sara Intikhab</p>
                        <p>Social Development and Policy</p>
                        <p>2022</p>
                    </div>
                    <div className="tab-row">
                        <p>Muhammad Ali Arif</p>
                        <p>Electrical Engineering	</p>
                        <p>2023</p>
                    </div>
                    <div className="tab-row">
                        <p>Muhammad Aqib Khan</p>
                        <p>Electrical Engineering</p>
                        <p>2023</p>
                    </div>
                    <div className="tab-row">
                        <p>Yabudullah Ahmed Bakhtiar</p>
                        <p>Electrical Engineering</p>
                        <p>2023</p>
                    </div>
                    <div className="tab-row">
                        <p>Asad Tariq</p>
                        <p>Social Development and Policy</p>
                        <p>2023</p>
                    </div>
                    <div className="tab-row">
                        <p>Mohammad Hasan Tariq</p>
                        <p>Social Development and Policy</p>
                        <p>2023</p>
                    </div>
                </div>
            </div>
        </section>

    </>
    );
}

export default Teams;