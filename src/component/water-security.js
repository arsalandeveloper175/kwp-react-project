import React from "react";
//Owl Carousel Libraries and Module
import OwlCarousel from 'react-owl-carousel';  
import 'owl.carousel/dist/assets/owl.carousel.css';  
import 'owl.carousel/dist/assets/owl.theme.default.css';  
import { Link } from 'react-router-dom';

const WaterSecurity = () =>{
    const supportgrantinner = {
        loop:false,
        margin:10,
        dots: false,
        autoplay:false,
        autoplayTimeout:2000,
        autoplayHoverPause:false,
        stagePadding:5,
        mouseDrag: false,
        responsive:{
            0:{
                items:1,
                stagePadding:5,
                dots:true,
            }, 
            480:{
                items:1,
                stagePadding:5,
                dots:true,
            },
            577:{
                items:1,
                stagePadding:5,
                dots:true,
            },
            767:{
                items:2,
                stagePadding:5,
                dots:true,
            },
            992:{
                items:2,
                stagePadding:0,
            }
        }
    }
    return(
        <>

            <div className="inner-page-banner">
                <div className="container-fluid p-0">
                    <img src="../img/project/project2.jpg" alt="" className="inner-banner img-fluid"/>
                </div>
            </div>

            <div className="project-deve-cont">
                <div className="container">
                    <div className="proj-inner-cont">
                        <h3>Water security through a political-ecological lens: A case study <br/> of Lyari Township</h3>
                        <p>In traditional water resource management, studies to estimate the reliability of water supply tend to remain “apolitical”, i.e., there is little to no mention of power asymmetries that create differential bargaining power in unequal societies.</p>
                        <p>Water security requires not only ensuring water availability, but also ensuring citizens’ “access” to the water supply. In Karachi’s context, the myopic focus on increasing water supplies often fails to consider social inequalities that render large portions of the population water insecure. This study assesses water scarcity using a political-ecological lens, through a case study of Lyari, a low-income and multiethnic township situated at the tail-end of the city’s water supply infrastructure and suffering from acute water scarcity. By employing a mixed-methods approach, we combine household surveys (selected through purposive sampling), interviews with relevant stakeholders, and participant observations to investigate tangible and intangible factors that influence the households’ ability to purchase, access, and consume a safe and adequate volume of water for domestic purposes.</p>
                    </div>

                </div>
            </div>

            <section className="projects-area inner-proj">
                <div className="container">
                    <div className="main-headings">
                        <h1>
                            <span>Other</span>
                            On going Projects
                        </h1>
                    </div>
                    <OwlCarousel  className="row justify-content-between coomon-responsove-slider owl-carousel" {...supportgrantinner}>
                        <div className="projec-caro-item pad-set">
                            <div className="proj-box">
                                <Link to="/Projects/DevelopLowCost" className="pro-links">
                                    <img src="../img/project1.jpg" alt="" className="prj-img img-fluid"/>
                                    <p>Development of a low-cost smart flowmeter network for domestic consumers</p>
                                </Link>
                            </div>
                        </div>
                        <div className="projec-caro-item pad-set">
                            <div className="proj-box">
                                <Link to="/Projects/TowardSustainable" className="pro-links">
                                    <img src="../img/project2.jpg" alt="" className="prj-img img-fluid"/>
                                    <p>Towards a sustainable and equitable water pricing strategy <br/> for Karachi</p>
                                </Link>
                            </div>
                        </div>
                    </OwlCarousel>
                </div>
            </section>

        </>
    );
}

export default WaterSecurity;