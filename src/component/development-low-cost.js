import React from "react";
//Owl Carousel Libraries and Module
import OwlCarousel from 'react-owl-carousel';  
import 'owl.carousel/dist/assets/owl.carousel.css';  
import 'owl.carousel/dist/assets/owl.theme.default.css';  
import { Link } from 'react-router-dom';

const DevelopLowCost = () =>{
    const supportgrantinner = {
        loop:false,
        margin:10,
        dots: false,
        autoplay:false,
        autoplayTimeout:2000,
        autoplayHoverPause:false,
        stagePadding:5,
        mouseDrag: false,
        responsive:{
            0:{
                items:1,
                stagePadding:5,
                dots:true,
            }, 
            480:{
                items:1,
                stagePadding:5,
                dots:true,
            },
            577:{
                items:1,
                stagePadding:5,
                dots:true,
            },
            767:{
                items:2,
                stagePadding:5,
                dots:true,
            },
            992:{
                items:2,
                stagePadding:0,
            }
        }
    }
    return(
        <>

            <div className="inner-page-banner">
                <div className="container-fluid p-0">
                    <img src="../img/project/project1.jpg" alt="" className="inner-banner img-fluid"/>
                </div>
            </div>

            <div className="project-deve-cont">
                <div className="container">
                    <div className="proj-inner-cont">
                        <h3>Development of a low-cost smart flowmeter network for <br/> domestic consumers</h3>
                        <p>A major cause of the dysfunctional water management in Karachi is the high level of non-revenue water (NRW) in the system which prevents Karachi’s water utility (KWSB) from being financially sustainable.</p>
                        <p>Estimates for NRW in Karachi’s water system range from 35% to as high as 60%. Because bulk water suppliers and household connections are not metered, it is difficult to accurately gauge the share of physical NRW. A prerequisite for reducing NRW is improved water accounting which requires credible quantitative flow data. Data on domestic water flows is also needed to understand where and how much water is used across Karachi. While much of the discourse regarding Karachi’s water challenges focus on a perceived ‘shortage’, it is not informed by credible empirical analysis and is based on unverified assumptions.</p>
                        <p>In this study, we are designing cost-effective smart flowmeters, and then installing these flowmeters in a variety of households exhibiting a wide range of socioeconomic characteristics. Combined with the outputs from Study 3, this study will provide two key solutions to Karachi’s water challenges: (i) a quantification of spatial and temporal patterns in domestic water usage that will improve current demand estimates and projections and allow for more targeted investments in water network upgrades, and (ii) availability of technology and evidence based equitable water pricing instruments that will help increase revenue to allow KWSB to achieve financial sustainability.</p>
                    </div>

                </div>
            </div>

            <section className="projects-area inner-proj">
                <div className="container">
                    <div className="main-headings">
                        <h1>
                            <span>Other</span>
                            On going Projects
                        </h1>
                    </div>
                    <OwlCarousel  className="row coomon-responsove-slider owl-carousel" {...supportgrantinner}>  
                        <div className="projec-caro-item pad-set">
                            <div className="proj-box">
                                <Link to="/Projects/TowardSustainable" className="pro-links">
                                    <img src="../img/project2.jpg" alt="" className="prj-img img-fluid"/>
                                    <p>Towards a sustainable and equitable water pricing strategy <br/> for Karachi</p>
                                </Link>
                            </div>
                        </div>
                        <div className="projec-caro-item pad-set">
                            <div className="proj-box">
                                <Link to="/Projects/WaterSecurity" className="pro-links">
                                    <img src="../img/project3.jpg" alt="" className="prj-img img-fluid"/>
                                    <p>Water security through a political-ecological lens: A case study of Lyari Township</p>
                                </Link>
                            </div>
                        </div>
                    </OwlCarousel>
                </div>
            </section>

        </>
    );
}

export default DevelopLowCost;