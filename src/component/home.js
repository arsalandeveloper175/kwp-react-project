import React from "react";
//Owl Carousel Libraries and Module
import OwlCarousel from 'react-owl-carousel';  
import 'owl.carousel/dist/assets/owl.carousel.css';  
import 'owl.carousel/dist/assets/owl.theme.default.css';  
import { Link } from 'react-router-dom';

const Home = () =>{
    //Owl Carousel Settings
    const projectslider = {
        loop:false,
        margin:10,
        dots: false,
        autoplay:false,
        autoplayTimeout:2000,
        autoplayHoverPause:false,
        mouseDrag: false,
        responsive:{
            0:{
                items:1,
                stagePadding:5,
                dots:true,
            }, 
            480:{
                items:1,
                stagePadding:5,
                dots:true,
            },
            577:{
                items:1,
                stagePadding:5,
                dots:true,
            },
            767:{
                items:2,
                stagePadding:5,
                dots:true,
            },
            992:{
                items:3,
                stagePadding:0,
            }
        }
    }
    const supportgrant = {
        loop:false,
        margin:10,
        dots: false,
        autoplay:false,
        autoplayTimeout:2000,
        autoplayHoverPause:false,
        stagePadding:5,
        mouseDrag: false,
        responsive:{
            0:{
                items:1,
                stagePadding:5,
                dots:true,
            }, 
            480:{
                items:1,
                stagePadding:5,
                dots:true,
            },
            577:{
                items:1,
                stagePadding:5,
                dots:true,
            },
            767:{
                items:2,
                stagePadding:5,
                dots:true,
            },
            992:{
                items:3,
                stagePadding:0,
            }
        }
    }
    
  return(
    <>
       
        <section className="about-area">
            <div className="container">
                <div className="row justify-content-center">
                    <div className="col-lg-8">
                        <div className="abt-content">
                            <h1 className="sec-headings">About us</h1>
                            <p>The Karachi Water Project (KWP) is an interdisciplinary research group engaged in investigating approaches to improve water management in Karachi. The group, headed by Dr. Hassaan F. Khan, strives to develop technological and policy-based solutions to Karachi’s water challenges.</p>
                        </div>
                    </div>
                </div>
            </div>
        </section>

        <section className="projects-area">
            <div className="container">
                <div className="main-headings">
                    <h1>
                        <span>On going </span>
                        Projects
                    </h1>
                </div>
                <OwlCarousel  className="row coomon-responsove-slider owl-carousel" {...supportgrant}>  
                    <div className="projec-caro-item">
                        <div className="proj-box">
                            <Link to="/Projects/DevelopLowCost" className="pro-links">
                                <img src="img/project1.jpg" alt="" className="prj-img img-fluid"/>
                                <p>Development of a low-cost smart flowmeter network for domestic consumers</p>
                            </Link>
                        </div>
                    </div>
                    <div className="projec-cao-item">
                        <div className="proj-box">
                            <Link to="/Projects/TowardSustainable" className="pro-links">
                                <img src="img/project2.jpg" alt="" className="prj-img img-fluid"/>
                                <p>Towards a sustainable and equitable water pricing strategy for Karachi</p>
                            </Link>
                        </div>
                    </div>
                    <div className="projec-cao-item">
                        <div className="proj-box">
                            <Link to="/Projects/WaterSecurity" className="pro-links">
                                <img src="img/project3.jpg" alt="" className="prj-img img-fluid"/>
                                <p>Water security through a political-ecological lens: A case study of Lyari Township</p>
                            </Link>
                        </div>
                    </div>
                </OwlCarousel>  
            </div>
        </section>


        <section className="team-area">
            <div className="container">
                <div className="team-cont">
                    <h1 className="sec-headings">The Team</h1>
                    <Link to="/Teams" className="see-member">Sea All Member</Link>
                </div>
            </div>
        </section>


        <section className="support-area">
            <div className="container">
                <div className="main-headings">
                    <h1>
                        <span>supported through</span>
                        grants
                    </h1>
                </div>
                <OwlCarousel  className="row coomonone-responsove-slider owl-carousel" {...projectslider}>  
                    <div className="suuport-gran-item">
                        <div className="support-box">
                        <a href="https://habib.edu.pk/" target="_blank" className="grant-links">
                            <img src="img/hu-logo.svg" alt="" className="supp-img"/>
                                <p>HU Internal Research Grant</p>
                        </a> 
                        </div>
                    </div>
                    <div className="suuport-gran-item">
                        <div className="support-box">
                            <a href="https://hec.gov.pk/english/pages/home.aspx" target="_blank" className="grant-links">
                                <img src="img/hec-logo.svg" alt="" className="supp-img"/>
                                <p>National Research Program for <br/> Universities - HEC Pakistan</p>
                            </a> 
                        </div>
                    </div>
                    <div className="suuport-gran-item">
                        <div className="support-box">
                            <a href="https://hashoofoundation.org/" target="_blank" className="grant-links">
                                <img src="img/hashoo-logo.svg" alt="" className="supp-img"/>
                                <p>Hashoo Foundation</p>
                            </a> 
                        </div>
                    </div>
                </OwlCarousel>
            </div>
        </section>
    </>
  );
}

export default Home;