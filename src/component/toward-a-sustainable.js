import React from "react";
//Owl Carousel Libraries and Module
import OwlCarousel from 'react-owl-carousel';  
import 'owl.carousel/dist/assets/owl.carousel.css';  
import 'owl.carousel/dist/assets/owl.theme.default.css';  
import { Link } from 'react-router-dom';

const TowardSustainable = () =>{
    const supportgrantinner = {
        loop:false,
        margin:10,
        dots: false,
        autoplay:false,
        autoplayTimeout:2000,
        autoplayHoverPause:false,
        stagePadding:5,
        mouseDrag: false,
        responsive:{
            0:{
                items:1,
                stagePadding:5,
                dots:true,
            }, 
            480:{
                items:1,
                stagePadding:5,
                dots:true,
            },
            577:{
                items:1,
                stagePadding:5,
                dots:true,
            },
            767:{
                items:2,
                stagePadding:5,
                dots:true,
            },
            992:{
                items:2,
                stagePadding:0,
            }
        }
    }
    return(
        <>


        <div className="inner-page-banner">
            <div className="container-fluid p-0">
                <img src="../img/project/project3.png" alt="" className="inner-banner img-fluid"/>
            </div>
        </div>
        
        <div className="project-deve-cont">
            <div className="container">
                <div className="proj-inner-cont">
                    <h3>Towards a sustainable and equitable water pricing strategy for Karachi</h3>
                    <p>The Karachi Water and Sewerage Board (KWSB) is stuck in a vicious cycle: low revenue collection restricts its ability to maintain and expand the water system; the resultant poor performance hinders any effort to increase revenues from consumers.</p>
                    <p>Major reasons for low revenue include high non-revenue water due to theft and leakage, low bill collection rates and tariffs for piped water that don’t account for actual usage. There’s also extreme inequity in water costs; on the one hand, there are communities that are suffering from acute water scarcity while also paying large sums out of their monthly income on water, on the other hand, there are neighborhoods that receive reliable piped water supply at a minimal or no charge. In this environment, KWSB needs help with improving its service delivery, and a pricing strategy that is not only equitable and inclusive—in terms of its billing structure for different income groups—but also sustainable in the long run—in terms of revenue sufficiency. This research study uses a combination of willingness-to-pay (WTP) surveys and economic models to explore the effectiveness of various water pricing strategies. Using long-term budgetary valuations for KWSB under different objectives and tariff regimes, we will devise a portfolio of water pricing strategies adapted for various contexts across Karachi.</p>
                </div>

            </div>
        </div>

        <section className="projects-area inner-proj">
            <div className="container">
                <div className="main-headings">
                    <h1>
                        <span>Other</span>
                        On going Projects
                    </h1>
                </div>
                
                <OwlCarousel  className="row justify-content-between  coomon-responsove-slider owl-carousel" {...supportgrantinner}>
                    <div className="projec-caro-item pad-set">
                        <div className="proj-box">
                            <Link to="/Projects/DevelopLowCost" className="pro-links">
                                <img src="../img/project1.jpg" alt="" className="prj-img img-fluid"/>
                                <p>Development of a low-cost smart flowmeter network for domestic consumers</p>
                            </Link>
                        </div>
                    </div>
                    <div className="projec-caro-item pad-set">
                        <div className="proj-box">
                            <Link to="/Projects/WaterSecurity" className="pro-links">
                                <img src="../img/project3.jpg" alt="" className="prj-img img-fluid"/>
                                <p>Water security through a political-ecological lens: A case study of Lyari Township</p>
                            </Link>
                        </div>
                    </div>
                </OwlCarousel>
            </div>
        </section>


    </>
    );
}

export default TowardSustainable;