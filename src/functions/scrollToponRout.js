// ScrollToTop.jsx   when ever i route page. Page will start from top
import { useEffect } from "react";
import { useLocation } from "react-router";

// This component use for Scroll Top on route Page
const ScrollToTop = (props) => {
  const location = useLocation();
  console.log(location);
  useEffect(() => {
    window.scrollTo(0, 0);
  }, [location]);

  return <>{props.children}</>
};
// This component use for Scroll Top on route Page

export default ScrollToTop;
