import Header from './shared/header';
import 'bootstrap/dist/css/bootstrap.min.css';
import Home from './component/home';
import Footer from './shared/footer';
import Teams from './component/teams';
import TowardSustainable from './component/toward-a-sustainable';
import DevelopLowCost from './component/development-low-cost';
import WaterSecurity from './component/water-security';
import {BrowserRouter, Switch, Route, } from "react-router-dom";
import ScrollToTop from './functions/scrollToponRout';

function App() {
  return (
    <div className="MainWrapper">
        {/* <BrowserRouter basename='/kwp-demo/react-project'>   */}{/*For Production Base Url Set For sub directory*/}
           <BrowserRouter> 
            <Header/>
            <ScrollToTop> {/*This component use for Scroll Top on route Page*/}
              <Switch>
                <Route exact path="/" component={Home}/>
                <Route path="/Teams" component={Teams} />
                    {/* Project Sub Menu */}
                      <Route path="/Projects/DevelopLowCost" component={DevelopLowCost} />
                      <Route path="/Projects/WaterSecurity" component={WaterSecurity} />
                      <Route path="/Projects/TowardSustainable" component={TowardSustainable} />
                    {/* Project Sub Menu */}
                </Switch>
              </ScrollToTop>
            <Footer/>
          </BrowserRouter>
    </div>
  );
}

export default App;
