import React from "react";
import { Link } from 'react-router-dom';


const Footer = () =>{
    return(
    <footer>
        <div className="container">
            <div className="row justify-content-center">
                <div className="col-lg-5">
                    <div className="foot-cont">
                        <div className="footer-logo">
                            <Link to="/" className="kwp-footer-logo"><img src="img/kw-footer-logo.svg" alt=""/></Link>
                            <a href="https://habib.edu.pk/" target="_blank" className="kwp-footer-logo"><img src="img/hu-footer-logo.svg" alt=""/></a>
                        </div>
                        <p className="copy-rights">Copyright © KWP 2021 | <a href="https://habib.edu.pk/" target="_blank">Habib University</a></p>
                    </div>
                </div>
            </div>
        </div>
    </footer>
    
    );
}

export default Footer;