import React, { useState } from "react";
import Navbar from 'react-bootstrap/Navbar';
import NavDropdown from 'react-bootstrap/NavDropdown'
import Nav from 'react-bootstrap/Nav'
import { useLocation,Link } from 'react-router-dom';

const Header = () =>{

    // ADD AND REMOVE CLASS BY USESTATE HOOK

        // Create State For Switch
            const [sweitchToggled,setSweitchToggled] = useState(false);
        // Create State For Switch
        const ToggleSwitch = ()=>{
            // Now i will change the state whenever this function is called


            // i am going to do is  if sweitchToggled is true then make is false and and if not true then make it true
                sweitchToggled ? setSweitchToggled(false) : setSweitchToggled(true);
            // i am going to do is  if sweitchToggled is true then make is false and and if not true then make it true


            // for testing i will log in console
                console.log(sweitchToggled);
            // for testing i will log in console
        }

    // ADD AND REMOVE CLASS BY USESTATE HOOK


    // Add Remove Navigation Active Class
        //assigning location variable
        const location = useLocation();

        //destructuring pathname from location
        const { pathname } = location;

        //Javascript split method to get the name of the path in array
        const splitLocation = pathname.split("/");
    // Add Remove Navigation Active Class

    return(
        <header className="main-header">
        <div className="container-fluid">
            <Navbar expand="lg" className="main-nav">
                    <div className="logo">
                        <Navbar.Brand as={Link} to="/">
                            <img src="img/kwp-logo.svg" className="img-fluid kwp-logo" alt=""/>
                            <img src="img/kwp-logo-res.svg" className="kwp-logo-res" alt=""/>
                        </Navbar.Brand>
                        <a className="navbar-right-resp" href="https://habib.edu.pk/" target="_blank">
                            <img src="img/hu-logo.svg" alt=""/>
                        </a>
                     </div>
                     <div className="nav-center">
                    <Navbar.Toggle aria-controls="navbarSupportedContent" onClick={ToggleSwitch}>
                        {/* if sweitchToggled is true then display a class and is not then  you can display or render another class 
                        or can even make it empty */}
                        <div id="nav-icon3" className={sweitchToggled ? "open" : "notopen"}>
                            <span></span>
                            <span></span>
                            <span></span>
                            <span></span>
                        </div>
                    </Navbar.Toggle>
                    <Navbar.Collapse id="navbarSupportedContent">
                        <Nav className="mr-auto">
                        <li className={splitLocation[1] === "" ? "active nav-item" : " nav-item"}>
                            <Nav.Link as={Link} to="/"><img src="img/home.svg" alt=""/> Home</Nav.Link>
                        </li>
                        <li className={splitLocation[1] === "Teams" ? "active nav-item" : "nav-item"}>
                            <Nav.Link as={Link} to="/Teams"><img src="img/team.svg" alt=""/> Team</Nav.Link>
                        </li>
                            <NavDropdown title={<div className="dopdown-title"><img src="img/projects.svg" alt=""/> Projects </div>} className={splitLocation[1] === "Projects" ? "active" : ""}>
                                <NavDropdown.Item as={Link} to="/Projects/DevelopLowCost" >Flowmeter Development</NavDropdown.Item>
                                <NavDropdown.Item as={Link} to="/Projects/WaterSecurity">Water in Lyari</NavDropdown.Item>
                                <NavDropdown.Item as={Link} to="/Projects/TowardSustainable">Water Pricing</NavDropdown.Item>
                            </NavDropdown>
                        <li className="nav-item">
                            <Nav.Link href="#link"><img src="img/data.svg" alt=""/> Data Portal</Nav.Link>
                        </li>
                        </Nav>
                    </Navbar.Collapse>
                    </div>
                    <div className="right-logo">
                        <a className="navbar-right-logo" href="https://habib.edu.pk/" target="_blank">
                        <img src="img/hu-header-logo.svg" alt=""/>
                        </a>
                    </div>
            </Navbar>
        </div>
      </header>
    );
}


export default Header;